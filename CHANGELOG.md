# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-10-15

### Added

- Add current user to sudoers.
- PyEnv libraries.

### Changed

- Update pandoc.
- Update Teams.
- Update Git.
- Clean Vagrant version number.
- Update VirtualBox.
- Update NVIDIA driver.
- Update Android Studio.
- Update Blender.
- Update LibreOffice.
- Update Zotero.
- Enable Smart Remove in Back In Time settings, so certain backups from the past are kept.

## [1.1.0] - 2022-06-16

### Added

- Startup Disk Creator.
- Vagrant.
- Desktop role tasks from base role.
- Document scanner.
- Variables file with software version numbers.

### Changed

- DVC installation method.
- Set software versions for tools role.
- Set software versions for latex role.
- Update to Blender 3.2.0.
- Set software versions for blender role.
- Set software versions for backup role.
- Set software versions for dvc role.
- Set software versions for zotero role.
- Set software versions for base role.

### Fixed

- Pygments version.
- Remove redundant Powerline fonts installation step.

## [1.0.0] - 2022-06-03

### Added

- Backup restoration instructions.

## [0.5.0] - 2022-05-31

### Added

- Backups with [Back In Time][backintime].
- Add new extra step for Firefox.
- Disable Firefox warn on quit.
- Firefox configuration.
- DVC public key.
- DVC.
- Set Git default branch name.
- Keyboard shortcuts.
- [VM acceleration][vm_acceleration] for Android Studio emulators.
- User's shell scripts.

[vm_acceleration]: https://developer.android.com/studio/run/emulator-acceleration#vm-linux "Configure VM acceleration on Linux"
[backintime]: https://github.com/bit-team/backintime "Back In Time - A simple backup tool for Linux"

## [0.4.0] - 2022-05-28

### Added

- Zotero.
- OneDrive.
- Let Android Studio work with USB-connected Android devices.
- Basic password management.
- eBook readers (Calibre ad Foliate).
- Configure multiple desktops.
- Install VirtualBox Extension Pack.
- User avatar and configuration.

### Changed

- Streamline pictures management.
- Update Android Studio.
- Add Blender's startup preferences.

## [0.3.0] - 2022-05-23

### Added

- Visual Studio Code terminal font.
- Powerlevel10k Zsh theme configuration.
- Zsh as default shell.
- Command z.
- Python and pyenv.

### Changed

- Move terminal-related tasks to their own role.

### Fixed

- Set Zsh as default shell.
- Python 3 pip's installation.
- Ansible's Python location when using pyenv.
- Aliases in .bashrc now have their own marker.

## [0.2.0] - 2022-05-23

### Added

- Disable SATA ACPI.
- Don't show Visual Studio Code's welcome screen.
- Single click to open folders in Nautilus.
- LaTeX.
- Show folders before files in Nautilus.
- Pandoc.
- VirtualBox.
- LibreOffice.
- VLC.
- Transmission.
- Inkscape.
- GIMP.

## [0.1.0] - 2022-05-22

### Added

- First complete installation cycle of some basic tools.
