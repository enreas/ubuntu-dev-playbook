# Ubuntu Developer Playbook

- Set versions for every single important software installed.
- Add Microsoft OneNote.
- Install Visual Studio Code extensions used in macOS.
- Include a virtual machine profile (Vagrant?) to easily test the playbook without installing everything from scratch.
