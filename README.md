# Ubuntu Developer Playbook

Ubuntu Developer setup and configuration via Ansible.

## Table of Contents

[TOC]

## Prerequisites

1. Download [Ubuntu Desktop][ubuntu_desktop_download].
2. Burn the ISO to a pen drive.
3. Boot from that pen drive.
4. Select the option **Try or Install Ubuntu**.
5. Click the button **Install Ubuntu**.
6. Choose your keyboard distribution.
7. Select **Minimal installation**.
8. Check **Download updates while installing Ubuntu**.
9. Select **Erase disk and install Ubuntu**.
10. Choose your location.
11. Provide your full name, user name, machine name and password. Don't auto login.
12. Complete the installation process.
13. Log in the machine.
14. Let Ubuntu update itself through APT and Snap, then reboot.
15. Install git: `sudo apt install git`.
16. Install [Ansible][ansible]: `sudo apt install ansible`
17. Clone [this repository][ubuntu-dev-playbook].
18. Don't forget to change to the repository's branch you need.
19. Copy all your SSH configuration and keys to the folder **files/ssh/**.
20. Copy your user's avatars folder with the name **Avatars** to the folder **files/user/Pictures/**. Make sure yours is named **avatar.png**.
21. Copy your user's desktop wallpapers folder with the name **Wallpapers** to the folder **files/user/Pictures/**.
22. Copy your user's shell scripts folder with the name **scripts** to the folder **files/user/scripts/**.
23. Connect an ext4-formatted USB drive with the name **Backups**. [Back In Time][backintime] will use it for the backups.

[ubuntu_desktop_download]: https://ubuntu.com/download/desktop "Download Ubuntu Desktop"
[ubuntu-dev-playbook]: https://gitlab.com/enreas/ubuntu-dev-playbook.git "Ubuntu setup and configuration via Ansible"
[ansible]: https://www.ansible.com/ "Simple IT Automation"
[backintime]: https://github.com/bit-team/backintime "Back In Time - A simple backup tool for Linux"

## Usage

1. Execute:

    ```shell
    ansible-playbook main.yml --ask-become-pass
    ```

2. Reboot the machine.

## Extra Steps

### BIOS

1. Enable **Peripherals > SW Guard Extensions (SGX)**.

### Terminal

1. Set the font **Meslo LG M for Powerline 12** in **Preferences > Profile > Text > Custom font**.

### Teams

1. Set **Settings > General > Theme** to **Dark**.
2. Uncheck **Settings > General > Application > Auto-start application**.
3. Uncheck **Settings > General > Application > On close, keep the application running**.

### Firefox

1. Go to **Passwords** and select the option **Import from a file** from the three-dotted menu in the upper-right corner.
2. Select the CSV file with the web passwords.
3. Import the digital certificates.
4. Install [Lean Library add-on][lean_library_firefox].
5. Install [Ghostery][ghostery].

> **Note**: There is a bug when [importing the passwords][bug_importing]. A workaround consists on visiting `about:config`, setting both `security.allow_eval` entries to `true`, importing the CSV file and setting both entries back to `false`.

[lean_library_firefox]: https://download.leanlibrary.com/bibliotecauex "Instálate Lean Library"
[ghostery]: https://addons.mozilla.org/en-US/firefox/addon/ghostery/ "Privacy Ad Blocker"
[bug_importing]: https://www.reddit.com/r/firefox/comments/xqt1kp/unable_to_import_chromes_password_csv_export_file/ "Unable to import Chrome's password .csv export file"

### GitKraken

1. Launch the application.
2. Click the button **Sign up with GitHub**.
3. Log in with the account GitKraken was register with.
4. Create your user profiles.
5. Set **Auto-Fetch Interval** to `0`.
6. Set the font **Meslo LG M for Powerline** in **Preferences > Terminal > Font**.
7. Disable local SSH agent for each profile.
8. Select private and public SSH keys for each profile.

### Python

1. Install Python 3.10.2 for Blender 3.1.2: `pyenv install 3.10.2`.
2. Install Python 3.8.10 for Ubuntu Server 20.04.4: `pyenv install 3.8.10`.

### PyCharm

1. Activate license with JB Account.
2. Check **Open Files with Single Click** in Project pane settings.
3. Check **Always Select Opened File** in Project pane settings.

### Android Studio

1. Launch Android Studio: `/opt/android-studio/bin/studio.sh`
2. Follow the Android Studio Setup Wizard steps.
3. Select **Tools > Create Desktop Entry** from the Android Studio menu bar to make Android Studio available in your list of applications.
4. Check **Open Files with Single Click** in Project pane settings.
5. Check **Always Select Opened File** in Project pane settings.
6. Check **CMake** in **Settings > Appearance & Behaviour > System Settings > Android SDK**, inside the **SDK Tools** tab.

You can find Android Studio download archives [here][android_studio_download_archives].

[android_studio_download_archives]: https://developer.android.com/studio/archive "Android Studio download archives"

### OneDrive

1. Execute `onedrive` form the terminal.
2. Open the link shown.
3. Give the application the permissions requested.
4. Copy the URL of the blank page you are redirected to in the terminal.
5. Enable and start the service:

    ```shell
    systemctl --user enable onedrive
    systemctl --user start onedrive
    ```

### Zotero

1. Install [Zotero Connector for Firefox][zotero_firefox].
2. Login in **Edit > Preferences > Sync**.

[zotero_firefox]: https://www.zotero.org/start "Install the Zotero Connector for Firefox"

### Bluetooth

1. Connect the Bluetooth dongle.
2. Go to **Settings > Bluetooth**.
3. Connect the devices.

## Restore from Back In Time

If you've follow these instructions before, Ubuntu has been backing your user's folder up periodically. To restore one of them, follow these steps:

1. Update your system with APT (`sudo apt update`) and Snap (**Ubuntu Software**).
2. Launch Back In Time.
3. Select the backup you want to restore from the left-side list (called **Snapshots**).
4. Select your home folder in the main pane location text box.
5. Select **Restore > Restore** from the menu bar.
6. Back In Time will show you the command it has just executed. It will run in the background. Monitor your system, make sure you don't use it until **rsync** finish restoring your home folder.
