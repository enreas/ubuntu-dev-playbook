user_pref("browser.shell.checkDefaultBrowser", true);
user_pref("browser.startup.page", 3);
user_pref("browser.startup.homepage", "about:blank");
user_pref("browser.newtabpage.enabled", false);
user_pref("browser.warnOnQuit", false);
user_pref("signon.management.page.fileImport.enabled", true);
